(function (angular, _, moment) {
  'use strict';

  let app = angular.module('app', [
    'ngRoute',
    'ui.bootstrap',
    'responsiveness',
    'api.assigneeConfigurationApi',
    'assigneeGroupSelector',
    'engagementGridAssignees',
    'engagementGridFilters',
    'engagementData',
    'engagementGrid',
    'audit',
    'notes',
    'config.assigneeGroupConfig'
  ]);

  app.config(function ($routeProvider) {
    $routeProvider
      .when('/grid', {
        templateUrl: 'engagementGridPage.html'
      })
      .when('/audit', {
        templateUrl: 'auditPage.html'
      })
      .when('/help', {
        templateUrl: 'helpPage.html'
      })
      .when('/config', {
        templateUrl: 'config/configPage.html'
      })
      .otherwise('/grid');
  });

  app.component('navbar', {
    templateUrl: 'navbar.html',
    transclude: true,
    controller: function ($location, identity, setup) {
      this.path = $location.$$path;
      identity.then(ident => this.identity = ident);
      setup.then(data => this.instanceLabel = data.instanceLabel);
    }
  });

  app.service('dateUtil', function () {
    this.mDateOnly = function mDateOnly(date) {
      return moment(moment(date).format('YYYY-MM-DD'));
    };

    this.mWeekStart = function mWeekStart(date) {
      return this.mDateOnly(date).day(1);
    };
  });

  app.factory('identity', function ($http) {
    return $http.get('/api/identity').then(unwrapResponse);
  });

  app.factory('setup', function ($http) {
    return $http.get('/api/setup').then(unwrapResponse);
  });

  app.run(function ($rootScope, identity) {
    identity.then(function (data) {
      $rootScope.identity = data;
    });
    $rootScope.build = window.build;
  });

  function unwrapResponse(resp) {
    return resp.data;
  }
}(angular, _, moment));

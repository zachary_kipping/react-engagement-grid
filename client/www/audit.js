(function (angular, _, moment) {
  'use strict';

  const MAX_NOTES = 200;

  angular.module('audit', ['notes'])
    .component('auditLog', {
      templateUrl: 'auditLog.html',
      controller: function (notesStorage) {
        const auditLog = this;

        this.filter = {};

        this.search = function () {
          auditLog.visibleNotes = _(notesStorage.allNotes)
            .sortBy('asOf')
            .filter(function (note) {
              let f = auditLog.filter;
              return (!f.enteredBy || note.enteredBy.indexOf(f.enteredBy) >= 0) &&
                (!f.instructor || note.instructor.indexOf(f.instructor) >= 0) &&
                (!f.notes || note.notes.indexOf(f.notes) >= 0);
            })
            .value()
            .slice(-MAX_NOTES);
        };

        this.weekEnd = function (weekStart) {
          return moment(weekStart).add(6, 'days').format();
        };

        notesStorage.populate().then(auditLog.search);
      }
    });
}(angular, _, moment));

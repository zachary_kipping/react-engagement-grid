(function (angular, _) {
  'use strict';

  angular.module('config.assigneeGroupConfig', [
    'config.assigneeGroupConfigState',
    'config.assigneeConfigEditor',
    'config.groupConfigEditor'
  ])
    .component('assigneeGroupConfig', {
      templateUrl: 'config/assigneeGroupConfig.html',
      controller: function (assigneeGroupConfigState, assigneeConfigurationApi) {
        this.state = assigneeGroupConfigState;
        this.activeTab = 'assignees';
      }
    });

}(angular, _));

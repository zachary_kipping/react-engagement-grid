(function (angular, _) {
  'use strict';

  angular.module('config.groupConfigEditor', ['dndLists'])
    .component('groupConfigEditor', {
      templateUrl: 'config/groupConfigEditor.html',
      bindings: {
        groups: '=',
        saveInProgress: '<'
      },
      controller: function ($scope) {
        this.newGroupCounter = 1;

        this.addGroup = function addGroup() {
          const id = this.newGroupCounter++;
          if (!this.groups) {
            this.groups = [];
          }
          this.groups.push({ name: `New group ${id}`, id: '$$temp-' + id });
        }

        this.removeGroup = function removeGroup(id) {
          this.groups = _.reject(this.groups, { id });
        }
      }
    });
}(angular, _));

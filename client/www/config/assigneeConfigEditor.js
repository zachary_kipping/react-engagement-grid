(function (angular, _) {
  'use strict';

  angular.module('config.assigneeConfigEditor', [])
    .component('assigneeConfigEditor', {
      templateUrl: 'config/assigneeConfigEditor.html',
      bindings: {
        groups: '=',
        assignees: '=',
        saveInProgress: '<'
      },
      controller: function () {
        this.newAssigneeCounter = 1;

        this.addAssignee = function addAssignee() {
          this.assignees.push({ key: `new.assignee.${this.newAssigneeCounter++}`, groups: [] });
        };

        this.isInGroup = function isInGroup(assignee, group) {
          return assignee.groups && assignee.groups.indexOf(group.id) >= 0;
        };

        this.toggleHidden = function toggleHidden(assignee) {
          if (this.saveInProgress) {
            return;
          }
          assignee.hidden = !assignee.hidden;
        };

        this.toggleGroup = function toggleGroup(assignee, group) {
          if (this.saveInProgress) {
            return;
          }
          if (this.isInGroup(assignee, group)) {
            assignee.groups = _.reject(assignee.groups, ag => ag === group.id);
          } else {
            assignee.groups = (assignee.groups || []).concat([group.id]);
          }
        };
      }
    });
}(angular, _));

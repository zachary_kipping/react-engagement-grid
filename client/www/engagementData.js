(function (angular, _, moment) {
  'use strict';

  angular.module('engagementData', [])
    .service('engagementData', function ($http) {
      function transform(resp) {
        let classes = _.map(resp.data, function (issue) {
          let classInfo;

          function parseInstructors() {
            let instructors = [];

            function addInstructor(field) {
              if (issue[field + '_key']) {
                let instructor = {
                  key: issue[field + '_key'],
                  name: issue[field + '_displayName'],
                  email: issue[field + '_emailAddress'],
                };
                if (issue[field + '_Travel_value'] === 'Online Instructor') {
                  instructor.travel = 'online';
                } else if (issue[field + '_Travel_value'] === 'Approved to Book') {
                  if (issue[field + '_Travel_child'] === 'Booked') {
                    instructor.travel = 'booked';
                  } else {
                    instructor.travel = 'booking-needed';
                  }
                }
                instructors.push(instructor);
              }
            }

            addInstructor('Primary_Instructor');
            addInstructor('Second_Instructor');
            addInstructor('Third_Instructor');
            return instructors;
          }

          function array(key) {
            let result = [];
            for (let i = 0; i < 10; i++) {
              if (issue[key + '_' + i]) {
                result.push(issue[key + '_' + i]);
              }
            }
            return result;
          }

          function parseTrainingQuestion() {
            let result = {};
            let jsonStart;
            if (issue.Training_Question && issue.Training_Question.length > 0) {
              jsonStart = issue.Training_Question.indexOf('{');
              if (jsonStart >= 0) {
                result.question = issue.Training_Question.substring(0, jsonStart);
                Object.assign(result, JSON.parse(issue.Training_Question.substring(jsonStart)));
              } else {
                result.question = issue.Training_Question.substring(0, jsonStart);
              }
            }
            return result;
          }

          classInfo = {
            key: issue.Key,
            url: issue.Issue_URL,
            summary: issue.Summary,
            description: issue.Description,
            type: issue.Issue_Type,
            status: issue.Status,
            classInterest: array('Class__Interest_'),
            // Assuming the field is labeled like "Class (Purchased)", single choice
            classPurchased: issue.Class__Purchased_,
            workshopInterest: array('Workshop__Interest_'),
            hasDate: !!(issue.Date_Booked || issue.Date_Held),
            startDateString: issue.Date_Booked || issue.Date_Held,
            endDateString: undefined,
            startTime: issue.Start_Time,
            endTime: issue.End_Time,
            duration: issue.Class_Duration,
            customerName: issue.Customer_Name,
            booked: !!issue.Date_Booked,
            rank: issue.Rank,
            email: issue.Email,
            address: issue.Address,
            created: issue.Created,
            updated: issue.Updated,
            website: issue.Website,
            timeZone: issue.Time_Zone,
            keyContact: issue.Key_Contact,
            mailingList: issue.Mailing_List,
            phoneNumber: issue.Phone_Number,
            datesOffered: issue.Dates_Offered,
            leadSources: array('Lead_Source'),
            ticketsSeats: issue.Tickets_Seats,
            actualSets: issue.Actual_Seats,
            invoiced: {
              value: issue.Invoiced_value,
              child: issue.Invoiced_child
            },
            billingAddress: issue.Billing_Address,
            initialContact: issue.Initial_Contact,
            instructorNeeds: array('Instructor_Need'),
            trainingDodList: array('Training_DOD_List'),
            classLocationAddress: issue.Class_Location__Address_,
            classLocationRegion: issue.Class_Location__Region_,
            classType: array('Class_Type'),
            eventTimeZone: issue.Event_Time_Zone,
            billingEmail: issue.Billing_Email,
            billingContact: issue.Billing_Contact,
            purchaseOrder: issue.Purchase_Order__,
            trainingQuestion: parseTrainingQuestion(),
            salesState: issue.Sales_State,
            salesStateOnline: issue.Sales_State__Online_
          };

          if (classInfo.hasDate) {
            classInfo.startDate = moment(classInfo.startDateString);
            classInfo.endDate = moment(classInfo.startDateString)
              .add(classInfo.duration - 1, 'days');
            classInfo.endDateString = classInfo.endDate.format('YYYY-MM-DD');
          }

          classInfo.public = classInfo.type.match(/Public/);
          if (classInfo.classPurchased === 'Angular Boot Camp 2.0' ||
            classInfo.classInterest.indexOf('Angular Boot Camp 2.0') >= 0) {
            classInfo.angular2 = true;
          } else if (classInfo.classInterest.indexOf('JIRA User Academy') >= 0 ||
            classInfo.workshopInterest.indexOf('JIRA User Academy') >= 0 ||
            classInfo.type.match(/(JBC|JIRA)/)) {
            classInfo.jira = true;
          } else if (classInfo.classInterest.indexOf('Atlassian User Academy') >= 0 ||
            classInfo.workshopInterest.indexOf('Atlassian User Academy') >= 0) {
            classInfo.atlassianAcademy = true;
          } else if (classInfo.type.match(/(Angular|ABC)/)) {
            if (classInfo.summary.match(/A(ngular )?2/i)) {
              classInfo.angular2 = true;
            } else {
              classInfo.angular = true;
            }
          }
          classInfo.instructors = parseInstructors();
          if (classInfo.instructors.length === 0) {
            if (classInfo.angular || classInfo.angular2) {
              classInfo.instructors.push({
                key: '$$unassigned-angular',
                name: '(Unassigned Angular)',
                email: '$$unassigned-angular'
              });
            } else if (classInfo.atlassianAcademy || classInfo.jira) {
              classInfo.instructors.push({
                key: '$$unassigned-jira',
                name: '(Unassigned JIRA)',
                email: '$$unassigned-jira'
              });
            } else {
              classInfo.instructors.push({
                key: '$$unassigned-other',
                name: '(Unassigned)',
                email: '$$unassigned-other'
              });
            }
            classInfo.unassigned = true;
          }

          classInfo.online = classInfo.classType && classInfo.classType.indexOf('Online') >= 0 ||
            classInfo.classLocationRegion &&
            classInfo.classLocationRegion.toLowerCase().indexOf('online') >= 0;
          classInfo.classLocationRegion = classInfo.classLocationRegion &&
            classInfo.classLocationRegion.replace(/\s*\+?\s*online/i, '');

          return classInfo;
        });
        return {
          classes: classes
        };
      }

      this.get = function () {
        return $http.get('/api/classes').then(transform);
      };
    });

}(angular, _, moment));

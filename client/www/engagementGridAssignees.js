(function (angular, _) {
  'use strict';

  angular.module('engagementGridAssignees', [])
    .service('engagementGridAssignees', function () {
      this._assignees = [];
      this._myself = {};
      this._groups = [];

      this.populate = function (identity, { assignees, groups }, classes) {
        function getSetup(assignee) {
          let setup = _.find(assignees, { key: assignee.key });
          if (setup) {
            setup = angular.copy(setup);
            setup.name = assignee.name || setup.name || setup.key.replace(/\./g, ' ');
            setup.email = assignee.email || setup.email || setup.key;
          } else {
            setup = {
              key: assignee.key,
              name: assignee.name,
              email: assignee.email,
              hidden: false
            };
          }
          return setup;
        };

        const processedKeys = {};
        this._assignees = [];
        for (const c of classes) {
          for (const i of c.instructors) {
            let setup = getSetup(i);
            if (setup.hidden) {
              continue;
            }
            if (!processedKeys[i.key]) {
              this._assignees.push(setup);
              processedKeys[i.key] = true
            }
          }
        }
        for (const a of assignees) {
          if (!a.hidden && !processedKeys[a.key]) {
            this._assignees.push(getSetup(a));
            processedKeys[a.key] = true
          }
        }

        let email = identity.User.Email;
        this._myself = _.find(this._assignees, { email: email });
        if (this._myself) {
          this._myself.myself = true;
        } else {
          let name = email.replace(/@.*/, '').replace(/\./g, ' ');
          this._assignees.splice(0, 0, { name: name, email: email, myself: true });
        }
        this._groups = groups;
      };

      this.getAssignees = function () {
        return this._assignees;
      };

      this.getGroups = function getGroups() {
        return this._groups;
      }
    });

}(angular, _));

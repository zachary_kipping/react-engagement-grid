(function (window, angular) {
  'use strict';

  angular.module('responsiveness', [])
    .service('responsiveness', function () {
      this.smallScreen = () => window.innerWidth <= 960;
    });
}(window, angular));

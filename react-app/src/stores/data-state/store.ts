import * as types from '../action-types';

import { Data } from '../../app/data-types';

interface StateInterface {
  data: Data;
}

interface ActionInterface {
  type: string;
  payload: any;
}

export const setData = (data: any) => {
  return {
    type: 'SET_DATA',
    payload: data
  };
};

const defaultState: StateInterface = {
  data: <Data> {}
};

export default (state: StateInterface = defaultState, action: ActionInterface) => {
  switch (action.type) {
    case types.RETRIEVE_DATA_SUCCESS:
      return action.payload;
    case types.RETRIEVE_DATA_ERROR:
      return action.payload;
    default:
      return state;
  }
};

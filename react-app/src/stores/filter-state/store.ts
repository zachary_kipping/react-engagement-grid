import { clone } from 'lodash';

interface Action {
  type: string;
  payload: any;
}

interface AssigneeGroupItem {
  id?: number;
  name: string;
  enabled: boolean;
}

interface Groups {
  [id: string]: AssigneeGroupItem;
}

interface State {
  view: number;
  groups: Groups;
}

// IDEA/TODO :: Add another default group called 'unassigned'.
//              Would filter assignees without group (group prop == null)
const DefaultState: State = {
  view: 3,
  groups: {
    'everyone': { name: 'Everyone', enabled: true },
    'just-me': { name: 'Just me', enabled: false }
  },
};

export const setView = (view: number) => {
  return {
    type: 'SET_VIEW',
    payload: view
  };
};

export const addGroupItems = (groups: Array<any>) => {
  return {
    type: 'ADD_ITEMS',
    payload: groups
  };
};

export const toggleGroupItem = (id: {}) => {
  return {
    type: 'TOGGLE_GROUP_ITEM',
    payload: id
  };
};

export default (state: State = DefaultState, action: Action) => {
  let stateClone = clone(DefaultState);
  switch (action.type) {
    case 'ADD_ITEMS':
      action.payload.forEach((assignee: any) => {
        stateClone.groups[assignee.id] = { name: assignee.name, enabled: stateClone.groups['everyone'].enabled };
      });
      return stateClone;

    case 'SET_VIEW':
      stateClone.view = action.payload;
      return stateClone;

    case 'TOGGLE_GROUP_ITEM':
      switch (action.payload) {
        case 'everyone':
          // Sets all groups to enabled if everyone is checked
          Object.keys(stateClone.groups).map(key => {
            // Replace true with false if you only want the Everyone group to be checked.
            stateClone.groups[key].enabled = true;
          });
          // If you replace the true above with false, the line below is unnecessary
          stateClone.groups['just-me'].enabled = false;
          break;
        case 'just-me':
          // Sets all groups to be disabled other than just-me
          Object.keys(stateClone.groups).map(key => {
            stateClone.groups[key].enabled = false;
          });
          stateClone.groups['just-me'].enabled = true;
          break;
        default:
          let notEveryone = false;

          let enabledKeys = Object.keys(stateClone.groups).filter((key: string) => { return key !== 'everyone' && key !== 'just-me'; });
          // Prevent client from selecting no groups
          if (enabledKeys.filter((key: string) => stateClone.groups[key].enabled).length > 1 || !stateClone.groups[action.payload].enabled) {
            stateClone.groups[action.payload].enabled = !stateClone.groups[action.payload].enabled;
          }

          // Checks to see if every group is selected. If it is, enabled is true.
          // Otherwise, it's false
          enabledKeys.forEach((key: string) => {
            notEveryone = !stateClone.groups[key].enabled || notEveryone;
          });

          if (!stateClone.groups[action.payload].enabled && notEveryone) {
            stateClone.groups['everyone'].enabled = false;
          } else if (!notEveryone) {
            stateClone.groups['everyone'].enabled = true;
          }
          stateClone.groups['just-me'].enabled = false;
          break;
      }
      return stateClone;

    default:
      return state;
  }
};

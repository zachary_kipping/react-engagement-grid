import * as _ from 'lodash';
import * as types from '../action-types';

interface ActionInterface {
  type: string;
  payload: any;
}

interface NotesState {
  notes: Array<Object>;
  allNotes: Array<Object>;
}

const defaultState: NotesState = {
  notes: [],
  allNotes: []
};

export const populateNotes = (data: any) => {
  return {
    type: types.RETRIEVE_NOTES_REQUEST,
    payload: data
  };
};

const populate = (data: any) => {
  return {
    allNotes: data.payload,
    notes: _(data.payload)
      .sortBy('asOf')
      .reverse()
      .uniqBy((v: any) => {
        return v.week + (v.instructor ? '-' + v.instructor : '');
      })
      .value()
  };
};

export default (state: NotesState = defaultState, action: ActionInterface) => {
  switch (action.type) {
    case 'POPULATE_NOTES':
      return populate(action.payload);
    default:
      return state;
  }
};

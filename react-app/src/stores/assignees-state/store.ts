import * as types from '../action-types';
import { Assignee } from '../../app/data-types';

interface ActionInterface {
  type: string;
  payload: any;
}

interface StateInterface {
  assignees: Assignee[];
  groups: any[];
  myself: boolean;
  identity: any;
}

const defaultState: StateInterface = {
  assignees: [],
  groups: [],
  myself: false,
  identity: { User: {}, Rights: {} }
};

export default (state: StateInterface = defaultState, action: ActionInterface) => {
  switch (action.type) {
    case types.RETRIEVE_ASSIGNEES_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

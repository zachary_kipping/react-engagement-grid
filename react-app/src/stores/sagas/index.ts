import { fork, all } from 'redux-saga/effects';
import PopulateAssignees from './populate-assignees';
import PopulateNotes from './populate-notes';

export default function* runForeman() {
  yield all([
    fork(PopulateAssignees),
    fork(PopulateNotes)
  ]);
}
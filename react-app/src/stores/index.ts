import { createStore, combineReducers, ReducersMapObject, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/index';

import Data from './data-state/store';
import Filter from './filter-state/store';
import Notes from './notes-state/store';
import Assignees from './assignees-state/store';

const reducers: ReducersMapObject = {
  data: Data,
  filter: Filter,
  notesStorage: Notes,
  assignees: Assignees
};

const rootReducer = combineReducers(reducers);

export default function configureStore(initialState: {}) {
  const sagaMiddleware = createSagaMiddleware();
  return {
    ...createStore(rootReducer, initialState, applyMiddleware(sagaMiddleware)),
    runSaga: sagaMiddleware.run(rootSaga)
  };
}

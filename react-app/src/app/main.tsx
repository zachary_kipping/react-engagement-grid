import * as React from 'react';
import { Route, HashRouter as Router, Switch } from 'react-router-dom';
import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';

import NavMenu from './navbar';
import EngagementGrid from './views/engagement-grid';
import Audit from './views/audit';
import Help from './views/help';
import * as types from '../stores/action-types';

class ReRoute extends React.Component<RouteComponentProps<{}>, any> {
  componentWillMount() {
    this.props.history.push('/grid');
  }
  render() {
    return null;
  }
}
// Set up component with NavMenu
const renderComponent = (Component: any, props: RouteComponentProps<{}>) => {
  return (
    <div>
      <NavMenu {...props} />
      <Component {...props} />
    </div>
  );
};

class Main extends React.Component<any, any> {
  // Runs before rendering to retrieve necessary data
  componentWillMount() {
    this.props.setData();
    this.props.populateAssigneesThing(this.props.data.classes);
    this.props.populateNotes();
  }
  render() {
    return (
      <Router hashType={'hashbang'}>
        <Switch>
          <Route exact={true} path="/grid" render={(routeProps: RouteComponentProps<{}>) => renderComponent(EngagementGrid, routeProps)} />
          <Route exact={true} path="/audit" render={(routeProps: RouteComponentProps<{}>) => renderComponent(Audit, routeProps)} />
          <Route exact={true} path="/help" render={(routeProps: RouteComponentProps<{}>) => renderComponent(Help, routeProps)} />
          <Route exact={true} path="/*" component={ReRoute} />
        </Switch>
      </Router>
    );
  }
}

const mapDispatchToProps = (dispatch: Function) => {
  return {
    setData: () => dispatch({ type: types.RETRIEVE_DATA_REQUEST }),
    populateNotes: () => dispatch({ type: types.RETRIEVE_NOTES_REQUEST }),
    populateAssigneesThing: () => dispatch({ type: types.RETRIEVE_ASSIGNEES_REQUEST })
  };
};

const mapStateToProps = (state: any, ownProps: {}) => {
  return {
    ...ownProps,
    data: state.data
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);

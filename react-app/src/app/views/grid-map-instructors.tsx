import * as React from 'react';

interface MapInstructorsProps {
  instructors: any;
}

const MapInstructors = (props: MapInstructorsProps) => {
  return (
    <tr>
      <th key={0} className="date-cell">Week</th>
      {
        props.instructors.map((i: any, index: number) => {
          return (
            <th key={'iHead_' + index} className="text-center" style={{ minWidth: '100px', width: (96 / props.instructors.length) + '%' }}>{i.name}</th>
          );
        })
      }
      <th key={2}><div style={{ width: 'calc(100vw - (80px + ' + props.instructors.length + ' * 300px))' }} /></th>
    </tr>
  );
};

export default MapInstructors;
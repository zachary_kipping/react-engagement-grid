import * as React from 'react';

import { Assignee } from '../data-types';
import EngagementGridClass from './engagement-grid-class';
import Notes from './notes';

interface TableInstructorProps {
  passed: {
    instructors: Array<any>;
    week: any;
    weeks: any;
    wIndex: any;
    needsHighlightYellow: Function;
    needsThickBorder: Function;
    viewSettings: any;
    classInstructor: Function;
    getClasses: Function;
    noteText: Function;
    isEditable: Function;
    saveNotes: Function;
    noteIndex: Array<any>;
    onClick: Function;
  };
}

const TableInstructor = (props: TableInstructorProps) => {
  const mapInstructors = () => {
    return props.passed.instructors.map((i: Assignee, iIndex: any) => {
      return (
        <td key={'1_' + iIndex} style={{ minWidth: '100px', width: (96 / props.passed.instructors.length) + '%' }}>
          {
            getClasses(i)
          }
          {
            !props.passed.week.weekStart || <Notes
              initialext={props.passed.noteText(props.passed.week, i)}
              editable={props.passed.isEditable(props.passed.week)}
              onChange={(text: any) => {
                props.passed.saveNotes(props.passed.week.key, props.passed.noteIndex[props.passed.week.key], text);
            }}/>
          }
        </td>
      );
    })
  }

  const getClasses = (i: any) => {
    props.passed.getClasses(props.passed.week, i).map((c: any, cIndex: any) => {
      return (
        <EngagementGridClass onClick={(event: any) => props.passed.onClick(props.passed.wIndex, event)} key={'c_' + props.passed.wIndex + '_' + iIndex + '_' + cIndex} classInfo={c} instructor={props.passed.classInstructor(c, i)} detailedView={props.passed.viewSettings.isExpanded(props.passed.wIndex)} />
      );
    })
  }

  const getWeekStart = () => {
    const wStart = new Date(props.passed.week.weekStart);
    const wEnd = new Date(props.passed.week.weekEnd);
    return wStart.getMonth() + '/' +
      wStart.getDate() + ' - ' +
      wEnd.getMonth() + '/' +
      wEnd.getDate();
  }

  return (
    <tr key={'w_' + props.passed.wIndex} className={(props.passed.week.year) ? 'year-header' : '' + (props.passed.week.past) ? ' past' : '' + (props.passed.needsThickBorder(props.passed.wIndex, props.passed.weeks)) ? ' thick-border' : '' + props.passed.needsHighlightYellow(props.passed.wIndex, props.passed.weeks) ? ' highlight-yellow' : ''}>
      {props.passed.week.year ? (
        <td colSpan={props.passed.instructors.length + 1}>
          {props.passed.week.year}
        </td>
      ) : ([
        (
          <td key={0} className="date-cell">
            <div onClick={(event: any) => props.passed.viewSettings.toggleRowExpansion(props.passed.wIndex, event)}>
              <div>
                {
                  props.passed.week.weekStart ?
                  (
                    <div>{getWeekStart()}</div>
                  ) : (
                    'No date'
                  )
                }
              </div>
            </div>
            {
              !props.passed.week.weekStart || 
              <Notes onChange={(text: any) => {
                props.passed.saveNotes(props.passed.week.key, null, props.passed.noteIndex[props.passed.week.key], text);
              }}/>
            }
          </td>
        ),
        ...mapInstructors(),
        (
          <td key={2}>
            <div style={{ width: 'calc(100vw - (80px + ' + props.passed.instructors.length + ' * 300px))' }} />
          </td>
        )]
      )}
    </tr>
  );
};

export default TableInstructor;
import * as React from 'react';
import { Glyphicon } from 'react-bootstrap';

const createSales = (salesStatus: string) => {
  return (
    <div>
      <span className="sales-border">
        <span className={'sales-inner sales-' + salesStatus} />
      </span>
    </div>
  );
};

const createIcon = (glyph: string) => {
  return (
    <div>
      <Glyphicon style={{ fontSize: '10px', color: '#888888' }} glyph={glyph} />
    </div>
  );
};

const handleSales = (sales: string) => {
  switch (sales) {
    case 'On Sale':
      return createSales('on-sale');
    case 'Almost Sold Out':
      return createSales('almost-sold-out');
    case 'Sold Out':
      return createSales('sold-out');
    case 'Draft':
      return createIcon('pencil');
    case 'Hidden':
      return createIcon('close');
    default:
      return '';
  }
};

const SalesIndicator = (props: { sales: string }) => {
  return (
    <div style={{ display: 'inline-block' }}>
      {handleSales(props.sales)}
    </div>
  );
};

export default SalesIndicator;

import * as React from 'react';
import * as ReactDOM from 'react-dom';

export default class Notes extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      content: this.props.text || this.props.initialText
    };
  }

  render() {
    if (!this.props.editable) {
      return (
        <span className="notes">{this.state.content}</span>
      );
    } else {
      return (
        <span className="notes"
          contentEditable={true}
          onInput={() => { this.props.onChange(ReactDOM.findDOMNode(this).innerHTML); }}>{this.state.content}
        </span>
      );
    }
  }
}

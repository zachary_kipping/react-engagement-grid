package main

import (
	"fmt"
	"net/http"

	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/user"
)

type InstructorSetup struct {
	ID       *datastore.Key `json:"id"`
	Key      string         `json:"key"`
	Name     string         `json:"name"`
	Email    string         `json:"email"`
	SortOder int            `json:"sortOrder"`
	Hidden   bool           `json:"hidden"`
}

func getInstructorSetup(c context.Context) ([]InstructorSetup, []*datastore.Key, error) {
	var instructors []InstructorSetup
	var keys []*datastore.Key
	keys, err := datastore.NewQuery("InstructorSetup").GetAll(c, &instructors)
	if err != nil {
		return nil, nil, err
	}

	return instructors, keys, nil
}

func migrate(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)
	if u == nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if !u.Admin {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	config, err := getAssigneeConfigurationFromDatastore(c)
	if err != nil {
		log.Errorf(c, "Error querying current configuration: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if len(config.Assignees) > 0 || len(config.Groups) > 0 {
		w.WriteHeader(http.StatusConflict)
		fmt.Fprintf(w, "Configuration already present")
		return
	}

	instructors, _, err := getInstructorSetup(c)
	if err != nil {
		log.Errorf(c, "Error querying instructors: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = datastore.RunInTransaction(c, migrateInTransaction(instructors), nil)
	if err != nil {
		log.Errorf(c, "Error mgirating data: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, "OK")
}

func migrateInTransaction(instructors []InstructorSetup) func(c context.Context) error {
	return func(c context.Context) error {
		_, err := createConfigurationRoot(c)
		if err != nil {
			return err
		}

		groups, err := insertDefaultGroups(c)
		if err != nil {
			return err
		}

		for _, instructor := range instructors {
			assignee := Assignee{Key: instructor.Key, Name: instructor.Name, Email: instructor.Email, Hidden: instructor.Hidden}
			switch instructor.SortOder {
			case 0:
				assignee.Groups = []*datastore.Key{groups[0].ID}
			case 10:
				assignee.Groups = []*datastore.Key{groups[1].ID}
			case 20:
				assignee.Groups = []*datastore.Key{groups[2].ID}
			case 50:
				assignee.Groups = []*datastore.Key{groups[3].ID}
			case 60:
				assignee.Groups = []*datastore.Key{groups[4].ID}
			case 1000:
				assignee.Groups = []*datastore.Key{groups[5].ID}
			}
			err = insertAssignee(c, &assignee)
			if err != nil {
				return err
			}
		}
		err = insertAssignee(c, &Assignee{Key: "$$unassigned-angular", Name: "(Unassigned Angular)", Groups: []*datastore.Key{groups[0].ID}})
		if err != nil {
			return err
		}
		err = insertAssignee(c, &Assignee{Key: "$$unassigned-jira", Name: "(Unassigned JIRA)", Groups: []*datastore.Key{groups[0].ID}})
		if err != nil {
			return err
		}
		err = insertAssignee(c, &Assignee{Key: "$$unassigned-other", Name: "(Unassigned)", Groups: []*datastore.Key{groups[0].ID}})
		if err != nil {
			return err
		}
		return nil
	}
}

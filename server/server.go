package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/user"
)

type Setup struct {
	InstanceLabel string `json:"instanceLabel"`
}

const ANONYMOUS = "Anonymous"

func init() {
	http.HandleFunc("/", getIndex)

	http.HandleFunc("/switch-account", switchAccount)

	http.HandleFunc("/migrate", migrate)

	http.HandleFunc("/setup-defaults", createDefaultAssigneeConfiguration)

	http.HandleFunc("/api/setup", getSetup)

	http.HandleFunc("/api/assignee-configuration", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			getAssigneeConfiguration(w, r)
			return
		case "POST":
			updateAssigneeConfiguration(w, r)
			return
		default:
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}
	})

	http.HandleFunc("/api/identity", getIdentity)

	http.HandleFunc("/api/classes", getClasses)

	http.HandleFunc("/api/notes", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			getNotes(w, r)
			return
		case "POST":
			saveNotes(w, r)
			return
		default:
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}
	})
}

func getIndex(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	u, err := getUserWithRights(c, w, r)
	if err != nil {
		log.Errorf(c, "Error getting user rights: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if u.Rights == nil && u.User.Email == ANONYMOUS {
		redirectToLogin(c, w, r)
		return
	}

	writeIndexHtml(c, w, r)
}

func redirectToLogin(c context.Context, w http.ResponseWriter, r *http.Request) {
	log.Infof(c, "Redirecting to login")
	url, err := user.LoginURL(c, r.URL.String())
	if err != nil {
		log.Errorf(c, "Error getting login URL: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Location", url)
	w.WriteHeader(http.StatusFound)
	return
}

func writeIndexHtml(c context.Context, w http.ResponseWriter, r *http.Request) {
	index, err := ioutil.ReadFile("react-app/build/index.html")
	if err != nil {
		log.Errorf(c, "Error loading index.html: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write(index)
}

func switchAccount(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	url, _ := user.LoginURL(c, r.Referer())
	// With passive=true, if the user has one account, they're silently redirected back (bad,
	// we want to switch)
	w.Header().Set("Location", strings.Replace(url, "passive=true", "passive=false", -1))
	w.WriteHeader(http.StatusFound)
}

func getSetup(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := auth(c, w, r)
	if u == nil {
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(
		Setup{
			InstanceLabel: os.Getenv("INSTANCE_LABEL"),
		})
}

func getIdentity(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := auth(c, w, r)
	if u == nil {
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(u)
}

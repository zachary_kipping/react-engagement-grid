package main

import (
	"encoding/json"
	"net/http"
	"os"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
	"google.golang.org/appengine/user"
)

type EngagementData map[string]interface{}

type FeedData struct {
	Data []EngagementData `json:"data"`
}

func getClasses(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := auth(c, w, r)
	if u == nil {
		return
	}

	if !u.Rights.SeeAllClasses && !u.Rights.SeeOwnClasses {
		http.Error(w, "You do not have the permission to see classes and notes", http.StatusUnauthorized)
		return
	}

	data, err := getFeedData(c)
	if err != nil {
		log.Errorf(c, "Error getting feed data: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	engagements, err := authorizeEngagements(data.Data, c, u)
	if err != nil {
		log.Errorf(c, "Error authorizing: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(engagements)
}

func getFeedData(c context.Context) (*FeedData, error) {
	client := urlfetch.Client(c)
	resp, err := client.Get(os.Getenv("DATA_URL"))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var data FeedData
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return nil, err
	}
	return &data, nil
}

func authorizeEngagements(data []EngagementData, c context.Context, u *UserWithRights) ([]EngagementData, error) {
	if u.Rights.SeeAllClasses {
		log.Debugf(c, "Can see all classes, returning as is")
		return data, nil
	}
	authorized := []EngagementData{}
	for _, engagement := range data {
		if !u.Rights.SeeAllClasses && !(u.Rights.SeeOwnClasses && isAssignedTo(c, engagement, u.User)) {
			log.Debugf(c, "Skipping %v - not assigned to %v", engagement["Key"], u.User.Email)
			continue
		}
		var date string
		if engagement["Date_Booked"] != nil {
			date = engagement["Date_Booked"].(string)
		} else if engagement["Date_Held"] != nil {
			date = engagement["Date_Held"].(string)
		}
		if date != "" && !(u.Rights.SeeFullHistory || isRecent(date)) {
			log.Debugf(c, "Skipping %v - not recent and user cannot see full history", engagement["Key"])
			continue
		}
		log.Debugf(c, "Authorized %v", engagement["Key"])
		authorized = append(authorized, engagement)
	}

	return authorized, nil
}

func isAssignedTo(c context.Context, e EngagementData, u *user.User) bool {
	log.Debugf(c, "Looking for %v in assignees: %v, %v, %v", u.Email, e["Primary_Instructor_emailAddress"], e["Second_Instructor_emailAddress"], e["Third_Instructor_emailAddress"])
	return e["Primary_Instructor_emailAddress"] == u.Email ||
		e["Second_Instructor_emailAddress"] == u.Email ||
		e["Third_Instructor_emailAddress"] == u.Email
}

func isRecent(date string) bool {
	parsed, err := time.Parse("2006-01-02", date)
	if err != nil {
		return false
	}
	duration := parsed.Sub(time.Now())
	return duration.Hours() > -30*24
}
